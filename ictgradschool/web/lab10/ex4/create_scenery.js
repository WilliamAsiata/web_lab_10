/**
 * Created by wasi131 on 8/05/2017.
 */
$(doLoad);

function doLoad() {
    var radio = $("input[type=radio]");
    radio.button();
    var i = 1;
    var ext = ".jpg";
    radio.each(function () {
        if (i == radio.length) ext = ".gif";
        radio.eq(i-1).change(changeBGImage("../images/ex4/background" + i++ + ext));
    });

    var checkboxes = $("input[type=checkbox]");
    checkboxes.button();
    i = 1;
    checkboxes.each(function () {
        var checkbox = $(this);

        var toggle = toggleDolphin(i++);
        if (checkbox.attr("checked") != "checked") toggle();
        checkbox.change(toggle);
    });

    var handle = $( "#custom-handle" );

    var slide = $( ".slider" );

    slide.slider({
        max: 200,
        slide: function( event, ui ) {
            scaleDolphins(ui.value);
        }
    });

    slide.slider("value", 100);
}

function changeBGImage(path) {
    return function(){
        $("#background").attr("src", path);
    };
}

function toggleDolphin(num) {
    return function () {
        var dolphin = $("#dolphin" + num);
        dolphin.toggleClass("invisible");
    }
}

function scaleDolphins(num) {
    console.log(num);
    var scale = num/100;
    $(".dolphin:not(.invisible)").css("transform","scale(" + scale + ")");
}