"use strict";

var bodyElement = $("body");
var divs = $("div");
divs.first().css("background-color", "pink");
/* 
 //One way to get the last p and make it red:
 var paragraphs = bodyElement.getElementsByTagName("p");
 lastParagraph = paragraphs[paragraphs.length-1];
 lastParagraph.style.color="red";
 */

//Another way, better:
var lastParagraph = $("#footer") //Note, getElementById is a method of the document node, not a method of any element nodes!!!
lastParagraph.css("color", "red");


divs.eq(1).hide();
/*
 // Another way. But unlike setting visibility, this way does not preserve the space allocated to the div (the 'block' display of divs):
 divs[1].style.display="none";
 */

// One way:
// we get back a list of all elements with class=subtitle, even though the list returned only happens to have one item this time.
var subtitles = $(".subtitle");
// get the first subtitle, get its first child (<small>), get its first child which is a textnode and then the latter's value
subtitles.children().before("Hello World");


var myButton = $("#makeBold");
//var myButtons = bodyElement.getElementsByTagName("button");
//console.log(myButtons[0]);

// When you click the button, set the style for all paragraphs. Accomplished without looping,
// uses createElement() to create a <style> element. See http://www.w3schools.com/jsref/dom_obj_style.asp
myButton.click(function () {
    // $("p").css("font-weight", "bold");
    // $("head").append("<STYLE></STYLE>");
    // $("p {font-weight: bold}").appendTo("STYLE");

    if ($("style").length == 0) $("head").append("<STYLE></STYLE>");
    $("style").html("p {font-weight: bold}");

    // var styleElement = document.createElement("STYLE");
    // styleElement.innerHTML = "p {font-weight: bold}";
    // document.head.appendChild(styleElement);




});

